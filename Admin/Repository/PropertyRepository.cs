﻿using Admin.Models;
using Admin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Del;
using System.Data;
using Admin.Helper;

namespace Admin.Repository
{
    public class PropertyRepository : IPropertyRepository
    {
        public bool CreateProperty(Property P)
        {
            var sql = "INSERT INTO rb_properties(siteid,propertyName,propertyAddress,cityName,stateName,zipCode,propertyType,propertyDetail,numberOfUnites,createDate,updateDate,STATUS)";
            sql += " VALUES(";
            sql += " 1,";
            sql += " '" + GeneralHelper.StrFormat(P.PropertyName.Trim()) + "',";
            sql += " '" + GeneralHelper.StrFormat(P.Address.Trim()) + "',";
            sql += " '" + GeneralHelper.StrFormat(P.CityName.Trim()) + "',";
            sql += " '" + GeneralHelper.StrFormat(P.State.Trim()) + "',";
            sql += " '" + GeneralHelper.StrFormat(P.ZipCode.Trim()) + "',";
            sql += GeneralHelper.StrFormat(P.PropertyType) + ",";
            sql += " '" + GeneralHelper.StrFormat(P.PropertyDetail.Trim()) + "',";
            sql += GeneralHelper.StrFormat(P.TotalNumberOfUnites) + ",";
            sql += "'" + GeneralHelper.FormatDateTime("yyyy-MM-dd HH:mm:ss") + "',";
            sql += "'" + GeneralHelper.FormatDateTime("yyyy-MM-dd HH:mm:ss") + "',";
            sql += GeneralHelper.StrFormat(P.Status) +")";

            var success = DataAccess.InsertNewRecord(sql);
            return success;
        }

        public bool UpdateProperty(int propertyId, Property P)
        {
            var sql = "Update rb_properties set siteid=1,";
            sql += "propertyName='" + GeneralHelper.StrFormat(P.PropertyName.Trim()) + "',";
            sql += "propertyAddress='" + GeneralHelper.StrFormat(P.Address.Trim()) + "',";
            sql += "cityName='" + GeneralHelper.StrFormat(P.CityName.Trim()) + "',";
            sql += "stateName='" + GeneralHelper.StrFormat(P.State.Trim()) + "',";
            sql += "zipCode='" + GeneralHelper.StrFormat(P.ZipCode.Trim()) + "',";
            sql += "propertyType=" + GeneralHelper.StrFormat(P.PropertyType) + ",";
            sql += "propertyDetail='" + GeneralHelper.StrFormat(P.PropertyDetail.Trim()) + "',";
            sql += "numberOfUnites=" + GeneralHelper.StrFormat(P.TotalNumberOfUnites) + ",";
            sql += "updateDate='" + GeneralHelper.FormatDateTime("yyyy-MM-dd HH:mm:ss") + "',";
            sql += "STATUS=" + GeneralHelper.StrFormat(P.Status);
            sql += " where id=" + GeneralHelper.StrFormat(propertyId);

            var success = DataAccess.ExecuteNonQuery(sql);
            return success;
        }

        public bool DeleteProperty(int propertyId)
        {
            var sql = "Update rb_properties set status=3 where id=" + GeneralHelper.StrFormat(propertyId);
            var success = DataAccess.ExecuteNonQuery(sql);
            return success;
        }

        public IEnumerable<Property> GetAllProperties()
        {
            List<Property> listProperties = new List<Property>();

            var sql = "select * from rb_properties where status=1 order by id desc";
            var result = DataAccess.GetData(sql);
            foreach(DataRow row in result.Rows)
            {
                Property property = new Property()
                {
                    PropertyId          = Convert.ToInt32(row["id"]),
                    PropertyName        = row["propertyName"].ToString(),
                    Address             = row["propertyAddress"].ToString(),
                    CityName            = row["cityName"].ToString(),
                    State               = row["stateName"].ToString(),
                    ZipCode             = row["zipCode"].ToString(),
                    PropertyDetail      = row["propertyDetail"].ToString(),
                    PropertyType        = Convert.ToInt32(row["propertyType"]),
                    TotalNumberOfUnites = Convert.ToInt32(row["numberOfUnites"]),
                    Status              = Convert.ToInt32(row["STATUS"])
                };

            listProperties.Add(property);
            }

            return listProperties;
        }

        public Property GetPropertyById(int id)
        {
            Property property = null;
            var sql = "select * from rb_properties where id=" + string.Format("{0}", id)+" order by id desc";
            var result = DataAccess.GetData(sql);
            if (result.Rows.Count > 0)
            {
                property = new Property();
                var row = result.Rows[0];
                property.PropertyId = Convert.ToInt32(row["id"]);
                property.PropertyName = row["propertyName"].ToString();
                property.Address = row["propertyAddress"].ToString();
                property.CityName = row["cityName"].ToString();
                property.State = row["stateName"].ToString();
                property.ZipCode = row["zipCode"].ToString();
                property.PropertyType = Convert.ToInt32(row["propertyType"]);
                property.PropertyDetail = row["propertyDetail"].ToString();
                property.TotalNumberOfUnites = Convert.ToInt32(row["numberOfUnites"]);
                property.Status = Convert.ToInt32(row["STATUS"]);
                property.ShowStatus = property.Status == 1 ? "Active" : "InActive";
            }

            return property;
        }

        
    }
}