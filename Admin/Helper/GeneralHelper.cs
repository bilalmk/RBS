﻿using Admin.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace Admin.Helper
{
    public static class GeneralHelper
    {
        public enum EStatusValue { Draft = 0, Active = 1, InActive = 2, Deleted = 3 };
        public enum EPropertyValue {None = 0, Residential = 1, Commercial = 2 }

        public static IEnumerable<SelectListItem> GetStatusList1(EStatusValue selectedStatus)
        {
            IEnumerable<EStatusValue> values = Enum.GetValues(typeof(EStatusValue)).Cast<EStatusValue>();

            IEnumerable<SelectListItem> items =
                from value in values
                select new SelectListItem
                {
                    Text = value.ToString(),
                    Value = value.ToString(),
                    Selected = value == selectedStatus,
                };

            return items;
        }

        public static SelectList GetStatusList(int selectedStatus)
        {
            List<Status> status = new List<Status>()
            {
                new Status() { ID = 0, Name = "Draft" },
                new Status() { ID = 1, Name = "Acitve" },
                new Status() { ID = 2, Name = "InActive" },
                new Status() { ID = 3, Name = "Delete" }
            };

            return new SelectList(status, "ID", "Name", selectedStatus);
        }

        public static SelectList GetPropertyTypeList(int selectedType)
        {
            List<Status> status = new List<Status>()
            {
                new Status() { ID = 1, Name = "Residential" },
                new Status() { ID = 2, Name = "Commercial" },
            };

            return new SelectList(status, "ID", "Name", selectedType);
        }

        public static string StrFormat(object Name)
        {
            return string.Format("{0}", Name);
        }

        public static String FormatDateTime(string Format)
        {
            return DateTime.Now.ToString(Format);
        }
    }
}