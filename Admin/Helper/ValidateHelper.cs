﻿using System.ComponentModel.DataAnnotations;

namespace Admin.Helper
{
    public static class ValidateHelper
    {
       
        public static bool IsValidEmail(string source)
        {
            return new EmailAddressAttribute().IsValid(source);
        }

        public static bool IsNumeric(string text)
        {
            return double.TryParse(text, out double test);
        }

    }
}