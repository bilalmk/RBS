﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Admin.Resources
{
    public class UserLoginResources
    {
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string LoginId { get; set; }

        [Required]
        public string Password { get; set; }
    }
}