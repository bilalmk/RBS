﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.Services
{
    public interface IPropertyRepository
    {
        bool CreateProperty(Property propertyModel);
        bool UpdateProperty(int propertyId,Property propertyModel);

        bool DeleteProperty(int propertyId);
        IEnumerable<Property> GetAllProperties();
        Property GetPropertyById(int id);
    }
}
