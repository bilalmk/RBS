﻿using Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.Services
{
    public interface IUsersRepository
    {
        Users ValidateLogin(string email, string password);
        IEnumerable<Users> List();

        Users FindByEmail(string email);
    }
}
