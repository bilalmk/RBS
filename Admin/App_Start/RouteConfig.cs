﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Admin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Dashboard",
                url: "Dashboard/{action}/{id}",
                defaults: new { Controller= "Dashboard", action="Index",id=UrlParameter.Optional }
                );

            routes.MapRoute(
                name: "Property",
                url: "Property/{action}/{id}",
                defaults: new { Controller = "Property", action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}
