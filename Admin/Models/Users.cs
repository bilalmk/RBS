﻿using Admin.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Admin.Models
{
    public class Users
    {
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string LoginId { get; set; }
        [Required]
        public string Password { get; set; }
        public Int32 SiteId { get; set; } 
        public Int32 UserUniqueId { get; }
        public Int32 RoleId { get; set; }
        public string UserName { get; set; }
        public string EmailAdress { get; set; }
        public string UserPhone { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime UpdateDateTime { get; set; }
        public DateTime LastLogin { get; set; }
        public Roles Roles { get; set; }
        public Int32 Status { get; set; }

    }
}