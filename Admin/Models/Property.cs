﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin.Models
{
    public class Property
    {

        public int PropertyId { get; set; }

        [Display(Name = "Property")]
        [Required(ErrorMessage = "Please enter property name.")]
        public string PropertyName { get; set; }

        [Required(ErrorMessage = "Please enter property address.")]
        public string Address { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please enter city name.")]
        public string CityName { get; set; }

        [Required(ErrorMessage = "Please enter state name.")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please enter zipcode.")]
        public string ZipCode { get; set; }

        [Display(Name = "Type")]
        public int PropertyType { get; set; }

        [Display(Name = "Detail")]
        public string PropertyDetail { get; set; }

        [Display(Name = "Unites")]
        [Range(1, int.MaxValue, ErrorMessage = "Only positive number allowed")]
        [Required(ErrorMessage = "Please enter positive number.")]
        public int TotalNumberOfUnites { get; set; }

        public string PropertyQuestion { get; set; }

        public int Status { get; set; }

        public string ShowStatus { get; set; }

    }
}