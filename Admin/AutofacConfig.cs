﻿using Admin.Repository;
using Admin.Services;
using Autofac;
using Autofac.Integration.WebApi;
using System.Reflection;
using System.Web.Http;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

namespace Admin
{
    public class AutofacConfig
    {

        public static IContainer Container;

        public static void Initialize()
        {
            Initialize(RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(IContainer container)
        {
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterSource(new ViewRegistrationSource());

            builder.RegisterType<UsersRepository>()
                   .As<IUsersRepository>()
                   .InstancePerRequest();

            builder.RegisterType<PropertyRepository>()
                   .As<IPropertyRepository>()
                   .InstancePerRequest();

            //Set the dependency resolver to be Autofac.
            var Container = builder.Build();

            return Container;
        }

    }
}