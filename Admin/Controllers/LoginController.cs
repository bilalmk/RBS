﻿using Admin.Models;
using Admin.Resources;
using Admin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Admin.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        private readonly IUsersRepository _usersRepository;

        public LoginController(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public ActionResult Index()
        {
            return View("Login", "~/Views/Shared/_Layout_login.cshtml");
        }

        [HttpPost]
        public ActionResult Login([Bind]Users userLogin)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrWhiteSpace(userLogin.LoginId) || !Helper.ValidateHelper.IsValidEmail(userLogin.LoginId))
                {
                    ViewBag.Message = "Please provide valid Login Id";
                    return View("Login", "~/Views/Shared/_Layout_login.cshtml");
                }
                if (string.IsNullOrWhiteSpace(userLogin.Password))
                {
                    ViewBag.Message = "Please provide valid Password";
                    return View("Login", "~/Views/Shared/_Layout_login.cshtml");
                }

                var users = _usersRepository.ValidateLogin(userLogin.LoginId, userLogin.Password);
                if (users != null)
                {
                    Session["loginId"] = userLogin.LoginId;
                    Session["siteId"] = userLogin.SiteId;
                    return Redirect("~/Dashboard");
                }
                else
                {
                    ViewBag.Message = "Please provid valid login information";
                }
                //return RedirectToAction("Index", "Home");
            }
            return View("Login", "~/Views/Shared/_Layout_login.cshtml");
            //return PartialView(userLogin);
        }

    }
}