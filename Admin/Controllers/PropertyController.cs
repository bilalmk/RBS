﻿using Admin.Library;
using Admin.Models;
using Admin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admin.Helper;

namespace Admin.Controllers
{
    public class PropertyController : Controller
    {
        private IPropertyRepository _propertyRepository;

        public PropertyController(IPropertyRepository propertyRepository)
        {
            _propertyRepository = propertyRepository;
        }


        // GET: Property
        public ActionResult Index()
        {
            List<Property> listProperties = new List<Property>();
            listProperties = _propertyRepository.GetAllProperties().ToList();

            if(TempData["errorMessage"] != null)
                ViewBag.Message = TempData["errorMessage"].ToString();

            return View(listProperties);
        }

        public ActionResult Create()
        {
            ViewBag.StatusList          = GeneralHelper.GetStatusList((int)GeneralHelper.EStatusValue.Active);
            ViewBag.PropertyTypeList    = GeneralHelper.GetPropertyTypeList((int)GeneralHelper.EPropertyValue.None);
            return View("Create");
        }


        [HttpPost]
        public ActionResult Create([Bind]Property propertyModel)
        {
            bool isError = false;
            
            if (string.IsNullOrWhiteSpace(propertyModel.PropertyName))
            {
                ViewBag.Message = "Please provide property name";
                isError = true;
            }
           else if (string.IsNullOrWhiteSpace(propertyModel.Address))
            {
                ViewBag.Message = "Please provide property address";
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(propertyModel.CityName))
            {
                ViewBag.Message = "Please provide city name";
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(propertyModel.State))
            {
                ViewBag.Message = "Please provide state name";
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(propertyModel.ZipCode))
            {
                ViewBag.Message = "Please provide zipcode";
                isError = true;
            }
            else if (propertyModel.TotalNumberOfUnites<=0)
            {
                ViewBag.Message = "Please provide valid number";
                isError = true;
            }

            if (isError)
            {
                return View();
            }

            var success = _propertyRepository.CreateProperty(propertyModel);

            if (!success)
            {
                ViewBag.Message = "Error occurred please try again";
                return View();
            }
            else
            {
                return Redirect("~/Property");
            }
        }

        public ActionResult PropertyDetail(int id)
        {
            if (!Helper.ValidateHelper.IsNumeric(Convert.ToString(id)))
            {
                ViewBag.Message = "Please provide valid Property id";
                return View();
            }

            var property = _propertyRepository.GetPropertyById(id);
            return View(property);
        }

        public ActionResult EditProperty(int id)
        {

            if (!Helper.ValidateHelper.IsNumeric(Convert.ToString(id)))
            {
                ViewBag.Message = "Please provide valid Property id";
                return View();
            }

            var property = _propertyRepository.GetPropertyById(id);

            if (property == null)
            {
                TempData["errorMessage"] = "Please provide valid Property id";
                return RedirectToAction("Index");
            }

            ViewBag.StatusList = GeneralHelper.GetStatusList(property.Status);
            ViewBag.PropertyType = GeneralHelper.GetPropertyTypeList(property.PropertyType);
            return View(property);
        }

        [HttpPost]
        public ActionResult EditProperty(Property propertyModel)
        {
            var urlArray = Request.Url.AbsolutePath.Split('/');
            var propertyId = urlArray[urlArray.Length-1];

            if (!Helper.ValidateHelper.IsNumeric(Convert.ToString(propertyId)))
            {
                ViewBag.Message = "Please provide valid Property id";
                return View();
            }

            bool isError = false;

            if (string.IsNullOrWhiteSpace(propertyModel.PropertyName))
            {
                ViewBag.Message = "Please provide property name";
                isError = true;
            }
            if (string.IsNullOrWhiteSpace(propertyModel.Address))
            {
                ViewBag.Message = "Please provide property address";
                isError = true;
            }
            if (string.IsNullOrWhiteSpace(propertyModel.CityName))
            {
                ViewBag.Message = "Please provide city name";
                isError = true;
            }
            if (string.IsNullOrWhiteSpace(propertyModel.State))
            {
                ViewBag.Message = "Please provide state name";
                isError = true;
            }
            if (string.IsNullOrWhiteSpace(propertyModel.ZipCode))
            {
                ViewBag.Message = "Please provide zipcode";
                isError = true;
            }
            if (propertyModel.TotalNumberOfUnites <= 0)
            {
                ViewBag.Message = "Please provide valid number";
                isError = true;
            }

            if (isError)
            {
                return View();
            }

            var success = _propertyRepository.UpdateProperty(Convert.ToInt32(propertyId), propertyModel);

            if (!success)
            {
                ViewBag.Message = "Error occurred please try again";
                return View();
            }
            else
            {
                ViewBag.Message = "Property update successfully";
                ViewBag.Status = GeneralHelper.GetStatusList(propertyModel.Status);
                ViewBag.PropertyType = GeneralHelper.GetPropertyTypeList(propertyModel.PropertyType);
                return View();
            }



        }

        public ActionResult DeleteProperty(int id)
        {
            if (!Helper.ValidateHelper.IsNumeric(Convert.ToString(id)))
            {
                ViewBag.Message = "Please provide valid Property id";
                return Redirect("~/Property");
            }

            var success = _propertyRepository.DeleteProperty(id);

            if (!success)
            {
                ViewBag.Message = "Error occurred please try again";

            }
            else
            {
                ViewBag.Message = "Property deleted successfully";
            }

            return Redirect("~/Property");
        }

        
    }
}