﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiModel;

namespace ApiContract
{
    public interface ITenantRepository
    {
        Tenant GetById(int tenantId);
        IEnumerable<Tenant> List(string buildingId);
        Tenant FindByEmail(string email);
        bool PasswordMatches(string providedPassword, string userPassword);
    }
}
