﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ApiContract;
using ApiModel;
using Del;


namespace ApiLibrary
{
    public class TenantRepository : ITenantRepository
    {
        public Tenant CreateTenant(Tenant tenantModel)
        {
            return new Tenant { FirstName = "Bilal", LastName = "Khan", Email = "bilalmk@gmail.com" };
        }

        public bool IsTenantExist(Tenant tenantModel)
        {
            string sql = "SELECT id FROM rb_tenant_info WHERE tenantName = '"+tenantModel.Name+"' OR tenantAddress = '"+tenantModel.TenantAddress+"' OR tenantEmail = '"+tenantModel.Email+"' OR tenantPhone = '"+tenantModel.OfficeNumber+"'";
            var data = DataAccess.GetData(sql);
            return true;
        }

        public Tenant GetById(int tenantId)
        {
            //TODO: Replace with actual implementation
            return new Tenant { TenantId = tenantId,  FirstName = "Muhammad", LastName = "Ali" };
        }

        public IEnumerable<Tenant> List(string buildingId)
        {
            //TODO: Replace with actual implementation
            return new List<Tenant> { GetById(1), GetById(2) };
        }

        public Tenant FindByEmail(string email)
        {
            return new Tenant { TenantId = 1, FirstName = "Saeed", LastName = "Siddiqui",Email=email };
        }

        public bool PasswordMatches(string providedPassword, string userPassword)
        {
            if (providedPassword == null || string.IsNullOrWhiteSpace(providedPassword))
            {
                return false;
            }

            if (userPassword == null || string.IsNullOrWhiteSpace(userPassword))
            {
                return false;
            }

            if (providedPassword == userPassword)
                return true;
            else
                return false;
        }
    }
}
