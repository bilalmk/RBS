﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Collections;
using System.Data;

namespace Del
{
    #region public enums

    public enum ComparisonOperator
    {
        Equal = 0,
        NotEqual = 1,
        Like = 2,
        NotLike = 3,
        StartWith = 4,
        EndWith = 5,
        GreaterThan = 6,
        LessThan = 7,
        GreaterThanOrEqual = 8,
        LessThanOrEqual = 9,
        IsNull = 10,
        IsNotNull = 11

    }

    public enum AndOrCondition
    {
        None = 0,
        EndCondition = 1,
        OrCondition = 2
    }

    public enum JoinType
    {
        InnerJoinStatement = 0,
        CrossJoinStatement = 1,
        LeftOuterJoinStatement = 2,
        RightOuterJoinStatement = 3
    }

    public enum StatementType
    {
        AllTypes = 0,
        Select = 1,
        Update = 2,
        Delete = 3
    }

    #endregion

    public class ConditionColumn
    {
        public string ConditionColumnTable { get; set; }
        public string ConditionColumnName { get; set; }
        public string ConditionColumnValue { get; set; }
        public AndOrCondition ConditionAndOr { get; set; }
        public StatementType ConditionFor { get; set; }
        public ComparisonOperator ConditionComparisonOperators { get; set; }

        public ConditionColumn()
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, string conditionColumnValue)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionTableName, string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator)
        {
            this.ConditionColumnTable = ConditionColumnTable;
            this.ConditionColumnName = ConditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, StatementType conditionFor)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, string conditionColumnValue, StatementType conditionFor)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator, StatementType conditionFor)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator, StatementType conditionFor)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = AndOrCondition.None;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, string conditionColumnValue, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, string conditionColumnValue, ComparisonOperator comparisonOperator, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = StatementType.AllTypes;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnValue, StatementType conditionFor, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = ConditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, string conditionColumnTable, string conditionColumnValue, StatementType conditionFor, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = ComparisonOperator.Equal;
        }

        public ConditionColumn(string conditionColumnName, ComparisonOperator comparisonOperator, string conditionColumnValue, StatementType conditionFor, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = string.Empty;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = comparisonOperator;
        }

        public ConditionColumn(string conditionColumnTable, string conditionColumnName, ComparisonOperator comparisonOperator, string conditionColumnValue, StatementType conditionFor, AndOrCondition conditionAndOr)
        {
            this.ConditionColumnTable = conditionColumnTable;
            this.ConditionColumnName = conditionColumnName;
            this.ConditionColumnValue = conditionColumnValue;
            this.ConditionAndOr = conditionAndOr;
            this.ConditionFor = conditionFor;
            this.ConditionComparisonOperators = comparisonOperator;
        }
    }

    public class JoinDetail
    {
        public JoinType JoinStatementType { get; set; }
        public string CurrentJoinTableName { get; set; }
        public string JoinTableName { get; set; }
        public string RequiredColumnName { get; set; }
        public string RequiredColumnDisplayName { get; set; }
        public string CurrentJoinColumnName { get; set; }
        public string JoinColumnName { get; set; }

        public JoinDetail()
        {
            this.JoinStatementType = JoinType.LeftOuterJoinStatement;
            this.CurrentJoinTableName = string.Empty;
            this.JoinTableName = string.Empty;
            this.RequiredColumnName = string.Empty;
            this.RequiredColumnDisplayName = string.Empty;
            this.CurrentJoinColumnName = string.Empty;
            this.JoinColumnName = string.Empty;
        }

        public JoinDetail(string joinTableName, string requiredColumnName, string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = JoinType.LeftOuterJoinStatement;
            this.CurrentJoinTableName = string.Empty;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = string.Empty;
            this.CurrentJoinColumnName = CurrentJoinColumnName;
            this.JoinColumnName = JoinColumnName;
        }

        public JoinDetail(JoinType joinType, string joinTableName, string requiredColumnName, string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = joinType;
            this.CurrentJoinTableName = string.Empty;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = string.Empty;
            this.CurrentJoinColumnName = CurrentJoinColumnName;
            this.JoinColumnName = JoinColumnName;
        }

        public JoinDetail(string joinTableName, string requiredColumnName, string requiredColumnDisplayName
                , string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = JoinType.LeftOuterJoinStatement;
            this.CurrentJoinTableName = string.Empty;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = requiredColumnDisplayName;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }

        public JoinDetail(JoinType joinType, string joinTableName, string requiredColumnName, string requiredColumnDisplayName
                           , string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = joinType;
            this.CurrentJoinTableName = string.Empty;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = requiredColumnDisplayName;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }

        public JoinDetail(string currentJoinTableName, string joinTableName, object requiredColumnName
                , string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = JoinType.LeftOuterJoinStatement;
            this.CurrentJoinTableName = currentJoinTableName;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName.ToString();
            this.RequiredColumnDisplayName = string.Empty;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }

        public JoinDetail(JoinType joinType, string currentJoinTableName, string joinTableName, object requiredColumnName
                           , string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = joinType;
            this.CurrentJoinTableName = currentJoinTableName;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName.ToString();
            this.RequiredColumnDisplayName = string.Empty;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }

        public JoinDetail(string currentJoinTableName, string joinTableName, string requiredColumnName
                       , string requiredColumnDisplayName, string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = JoinType.LeftOuterJoinStatement;
            this.CurrentJoinTableName = currentJoinTableName;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = requiredColumnDisplayName;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }


        public JoinDetail(JoinType joinType, string currentJoinTableName, string joinTableName, string requiredColumnName
            , string requiredColumnDisplayName, string currentJoinColumnName, string joinColumnName)
        {
            this.JoinStatementType = joinType;
            this.CurrentJoinTableName = currentJoinTableName;
            this.JoinTableName = joinTableName;
            this.RequiredColumnName = requiredColumnName;
            this.RequiredColumnDisplayName = requiredColumnDisplayName;
            this.CurrentJoinColumnName = currentJoinColumnName;
            this.JoinColumnName = joinColumnName;
        }
    }

    public class ColumnsDetail
    {
        public List<string> ColumnName { get; set; }
        public List<object> ColumnValue { get; set; }
        public List<string> ColumnDisplayName { get; set; }
        public List<JoinDetail> JoinDetails { get; set; }
        public List<ConditionColumn> ConditionColumns { get; set; }

        public List<string> EliminatedColumns { get; set; }

        public string SelectConditionString { get; set; }

        public string UpdateConditionString { get; set; }
        public string DeleteConditionString { get; set; }

        public ColumnsDetail()
        {
            this.ColumnName = new List<string>();
            this.ColumnValue = new List<object>();
            this.ColumnDisplayName = new List<string>();
            this.JoinDetails = new List<JoinDetail>();
            this.ConditionColumns = new List<ConditionColumn>();
            this.EliminatedColumns = new List<string>();
            this.SelectConditionString = string.Empty;
            this.UpdateConditionString = string.Empty;
            this.DeleteConditionString = string.Empty;
        }

        public ColumnsDetail(int columnNumber)
        {
            this.ColumnName = new List<string>(columnNumber);
            this.ColumnValue = new List<object>(columnNumber);
            this.ColumnDisplayName = new List<string>();
            this.JoinDetails = new List<JoinDetail>();
            this.ConditionColumns = new List<ConditionColumn>();
            this.EliminatedColumns = new List<string>();
            this.SelectConditionString = string.Empty;
            this.UpdateConditionString = string.Empty;
            this.DeleteConditionString = string.Empty;
        }

    }

    public static class DataAccess
    {
        #region General Function
        public static string connectionString = string.Empty;
        //private static MySqlConnection sqlConn  = new MySqlConnection(System.Configuration.ConfigurationManager.AppSettings['myConnectionString']);
        private static MySqlConnection sqlConn = new MySqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString);
        //private static MySqlConnection sqlConn = new MySqlConnection(connectionString);;

        private static MySqlCommand sqlCmd = new MySqlCommand();

        private static MySqlTransaction sqlTrns = null;

        private static bool UseTransaction { get; set; }

        public static string CurrentSqlStatment { get; set; }

        public static Hashtable CurrentSqlParameters = new Hashtable();

        

        public static ConnectionState CurrentConnectionState
        {
            get
            {
                return sqlConn.State;
            }
        }

        public static bool OpenConnection()
        {
            bool success = false;
            if (sqlConn.State != ConnectionState.Open)
            {
                CloseConnection();
                sqlConn.Open();
                success = true;
            }
            return success;
        }

        public static bool CloseConnection()
        {
            bool success = false;
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
                success = true;
            }
            return success;
        }

        public static bool BeginTransaction()
        {
            bool success = false;

            if (sqlTrns != null)
                CloseConnection();

            OpenConnection();

            sqlCmd = sqlConn.CreateCommand();
            sqlTrns = sqlConn.BeginTransaction(IsolationLevel.Serializable);
            sqlCmd.Connection = sqlConn;
            sqlCmd.Transaction = sqlTrns;
            UseTransaction = true;
            success = true;

            return success;
        }

        public static bool CommitTransaction()
        {
            bool success = false;
            if (sqlConn.State != ConnectionState.Closed)
            {
                if (sqlTrns != null)
                {
                    sqlTrns.Commit();
                    UseTransaction = false;
                    success = true;
                    sqlTrns = null;
                }
            }
            return success;
        }

        public static bool RollbackTransaction()
        {
            bool success = false;
            if (sqlConn.State != ConnectionState.Closed)
            {
                if (sqlTrns != null)
                {
                    sqlTrns.Rollback();
                    UseTransaction = false;
                    success = true;
                    sqlTrns = null;
                }
            }
            return success;
        }

        public static object GetSingle(string sqlQuery)
        {
            object obj = null;
            MySqlCommand cmd = null;
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();
                cmd = new MySqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                obj = cmd.ExecuteScalar();
            }
            catch (Exception err)
            {
                throw err;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
            }

            return obj;
        }

        public static bool ExecuteNonQuery(string sqlQuery)
        {
            bool success = false;
            MySqlCommand cmd = null;
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                cmd = new MySqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                if (cmd.ExecuteNonQuery() > 0)
                    success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
            }
            return success;
        }


        public static DataTable GetData(string sqlQuery)
        {
            MySqlCommand cmd = null;
            MySqlDataAdapter da = null;
            DataTable dt = null;
            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                cmd = new MySqlCommand();
                dt = new DataTable();
                cmd.Connection = sqlConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                da = new MySqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                da.Dispose();
                da = null;
            }
            return dt;
        }


        public static DataTable SelectRecords(string sqlQuery, string cacheKey, ref HttpApplicationState application, ref System.Web.Caching.Cache cache)
        {
            MySqlCommand cmd = null;
            MySqlDataAdapter da = null;
            DataTable result = null;

            string isCacheEnabled = System.Configuration.ConfigurationManager.AppSettings["CacheEnabled"] != null ? System.Configuration.ConfigurationManager.AppSettings["CacheEnabled"] : string.Empty;

            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                cmd = new MySqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                da = new MySqlDataAdapter(cmd);

                if (isCacheEnabled.ToLower() == "true" && !(cache.Get(cacheKey) is object))
                {
                    da.Fill(result);
                    cache.Insert(cacheKey, result);
                }
                else if (isCacheEnabled.ToLower() == "true" && cache.Get(cacheKey) is object)
                {
                    result = (DataTable)cache[cacheKey];
                }
                else
                {
                    da.Fill(result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                da.Dispose();
                da = null;
                result.Dispose();
                result = null;
            }
            return result;
        }

        public static DataTable SelectRecords(string sqlQuery)
        {
            MySqlCommand cmd = null;
            MySqlDataAdapter da = null;
            DataTable result = null;


            try
            {
                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                cmd = new MySqlCommand();
                cmd.Connection = sqlConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sqlQuery;
                da = new MySqlDataAdapter(cmd);
                da.Fill(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Dispose();
                cmd = null;
                da.Dispose();
                da = null;
                result.Dispose();
                result = null;
            }
            return result;
        }

        public static DataTable SelectRecords(string tableName, ColumnsDetail columns)
        {
            DataTable result = new DataTable();
            string sqlQuery = string.Empty;
            int parameterCount = 1;

            bool defaultConditionEntered = false;

            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 0)
            {
                MySqlCommand cmd = null;
                MySqlDataAdapter da = null;
                try
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = sqlConn;
                    cmd.CommandType = CommandType.Text;

                    sqlQuery = "Select ";

                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        if (columns.ColumnDisplayName.Count - i > 1 && columns.ColumnDisplayName[i] != string.Empty)
                            sqlQuery += string.Format("{0}.", tableName) + columns.ColumnName[i] + " as '" + columns.ColumnDisplayName[i] + "',";
                        else
                            sqlQuery += string.Format("{0}.", tableName) + columns.ColumnName[i] + " , ";
                    }

                    for (int i = 0; i < columns.JoinDetails.Count; i++)
                    {
                        if (columns.JoinDetails[i].RequiredColumnDisplayName == string.Empty)
                            sqlQuery += string.Format("{0}.", columns.JoinDetails[i].JoinTableName) + columns.JoinDetails[i].RequiredColumnName + " , ";
                        else
                            sqlQuery += string.Format("{0}.", columns.JoinDetails[i].JoinTableName) + columns.JoinDetails[i].RequiredColumnName + " as '" + columns.JoinDetails[i].RequiredColumnDisplayName + "',";
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));
                    sqlQuery += " FROM " + tableName + " ";

                    ArrayList usedJoinTable = new ArrayList();

                    for (int i = 0; i < columns.JoinDetails.Count; i++)
                    {
                        if (!usedJoinTable.Contains(columns.JoinDetails[i].JoinTableName))
                        {
                            usedJoinTable.Add(columns.JoinDetails[i].JoinTableName);
                            switch (columns.JoinDetails[i].JoinStatementType)
                            {
                                case JoinType.CrossJoinStatement:
                                    sqlQuery += " cross join ";
                                    break;
                                case JoinType.InnerJoinStatement:
                                    sqlQuery += " inner join ";
                                    break;
                                case JoinType.LeftOuterJoinStatement:
                                    sqlQuery += " left outer join ";
                                    break;
                                case JoinType.RightOuterJoinStatement:
                                    sqlQuery += " right outer join ";
                                    break;
                                default:
                                    sqlQuery += " inner join ";
                                    break;
                            }

                            sqlQuery += columns.JoinDetails[i].JoinTableName;

                            if (columns.JoinDetails[i].JoinStatementType == JoinType.InnerJoinStatement || columns.JoinDetails[i].JoinStatementType == JoinType.LeftOuterJoinStatement || columns.JoinDetails[i].JoinStatementType == JoinType.RightOuterJoinStatement)
                            {
                                if (columns.JoinDetails[i].CurrentJoinTableName == string.Empty)
                                    sqlQuery += " on " + string.Format("{0}.{1}", tableName, columns.JoinDetails[i].CurrentJoinColumnName);
                                else
                                    sqlQuery += " on " + string.Format("{0}.{1}", columns.JoinDetails[i].CurrentJoinTableName, columns.JoinDetails[i].CurrentJoinColumnName);

                                sqlQuery += " = " + string.Format("{0}.{1}", columns.JoinDetails[i].JoinTableName, columns.JoinDetails[i].JoinColumnName);
                            }
                        }
                    }

                    if (columns.SelectConditionString == null || columns.SelectConditionString == string.Empty)
                    {
                        foreach (ConditionColumn conditionColumns in columns.ConditionColumns)
                        {
                            if (conditionColumns.ConditionFor == StatementType.AllTypes || conditionColumns.ConditionFor == StatementType.Select)
                            {
                                sqlQuery += " where ";
                                break;
                            }
                        }

                        if (columns.ConditionColumns.Count > 1)
                        {
                            for (int i = 0; i < columns.ConditionColumns.Count; i++)
                            {
                                if (columns.ConditionColumns[i].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[i].ConditionFor == StatementType.Select)
                                {
                                    if (columns.ConditionColumns[i].ConditionColumnTable == string.Empty)
                                        sqlQuery += AddComparisonOperator(string.Format("{0}.", tableName) + columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, parameterCount, ref cmd);
                                    else
                                        sqlQuery += AddComparisonOperator(string.Format("{0}.", columns.ConditionColumns[i].ConditionColumnTable) + columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, parameterCount, ref cmd);
                                }

                                parameterCount++;

                                switch (columns.ConditionColumns[i].ConditionAndOr)
                                {
                                    case AndOrCondition.EndCondition:
                                        sqlQuery += " AND ";
                                        break;
                                    case AndOrCondition.OrCondition:
                                        sqlQuery += " OR ";
                                        break;
                                    default:
                                        sqlQuery += " AND ";
                                        defaultConditionEntered = true;
                                        break;
                                }
                            }
                            if (defaultConditionEntered == true)
                            {
                                sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf("AND"));
                            }
                        }
                        else if (columns.ConditionColumns.Count == 1)
                        {
                            if (columns.ConditionColumns[0].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[0].ConditionFor == StatementType.Select)
                            {
                                if (columns.ConditionColumns[0].ConditionColumnTable == string.Empty)
                                    sqlQuery += AddComparisonOperator(string.Format("{0}.", tableName) + columns.ConditionColumns[0].ConditionColumnName, columns.ConditionColumns[0].ConditionComparisonOperators, columns.ConditionColumns[0].ConditionColumnValue, parameterCount, ref cmd);
                                else
                                    sqlQuery += AddComparisonOperator(string.Format("{0}.", columns.ConditionColumns[0].ConditionColumnTable) + columns.ConditionColumns[0].ConditionColumnName, columns.ConditionColumns[0].ConditionComparisonOperators, columns.ConditionColumns[0].ConditionColumnValue, parameterCount, ref cmd);
                            }

                        }
                    }
                    else
                    {
                        sqlQuery += " " + columns.SelectConditionString;
                    }

                    cmd.CommandText = sqlQuery;
                    da = new MySqlDataAdapter(cmd);
                    da.Fill(result);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in cmd.Parameters)
                    {
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                    }
                    cmd.Dispose();
                    cmd = null;
                    da.Dispose();
                    da = null;
                }

            }
            return result;
        }

        public static DataTable SelectRecords(string tableName, ColumnsDetail columns, string cacheKey, ref HttpApplicationState application, ref System.Web.Caching.Cache cache)
        {
            DataTable result = new DataTable();
            string isCacheEnabled = System.Configuration.ConfigurationManager.AppSettings["CacheEnabled"] != null ? System.Configuration.ConfigurationManager.AppSettings["CacheEnabled"] : string.Empty;
            string sqlQuery = string.Empty;
            int parameterCount = 1;

            bool defaultConditionEntered = false;

            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 0)
            {
                MySqlCommand cmd = null;
                MySqlDataAdapter da = null;
                try
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = sqlConn;
                    cmd.CommandType = CommandType.Text;

                    sqlQuery = "Select ";

                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        if (columns.ColumnDisplayName.Count - i > 1 && columns.ColumnDisplayName[i] != string.Empty)
                            sqlQuery += string.Format("{0}.", tableName) + columns.ColumnName[i] + " as '" + columns.ColumnDisplayName[i] + "',";
                        else
                            sqlQuery += string.Format("{0}.", tableName) + columns.ColumnName[i] + " , ";
                    }

                    for (int i = 0; i < columns.JoinDetails.Count; i++)
                    {
                        if (columns.JoinDetails[i].RequiredColumnDisplayName == string.Empty)
                            sqlQuery += string.Format("{0}.", columns.JoinDetails[i].JoinTableName) + columns.JoinDetails[i].RequiredColumnName + " , ";
                        else
                            sqlQuery += string.Format("{0}.", columns.JoinDetails[i].JoinTableName) + columns.JoinDetails[i].RequiredColumnName + " as '" + columns.JoinDetails[i].RequiredColumnDisplayName + "',";
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));
                    sqlQuery += " FROM " + tableName + " ";

                    ArrayList usedJoinTable = new ArrayList();

                    for (int i = 0; i < columns.JoinDetails.Count; i++)
                    {
                        if (!usedJoinTable.Contains(columns.JoinDetails[i].JoinTableName))
                        {
                            usedJoinTable.Add(columns.JoinDetails[i].JoinTableName);
                            switch (columns.JoinDetails[i].JoinStatementType)
                            {
                                case JoinType.CrossJoinStatement:
                                    sqlQuery += " cross join ";
                                    break;
                                case JoinType.InnerJoinStatement:
                                    sqlQuery += " inner join ";
                                    break;
                                case JoinType.LeftOuterJoinStatement:
                                    sqlQuery += " left outer join ";
                                    break;
                                case JoinType.RightOuterJoinStatement:
                                    sqlQuery += " right outer join ";
                                    break;
                                default:
                                    sqlQuery += " inner join ";
                                    break;
                            }

                            sqlQuery += columns.JoinDetails[i].JoinTableName;

                            if (columns.JoinDetails[i].JoinStatementType == JoinType.InnerJoinStatement || columns.JoinDetails[i].JoinStatementType == JoinType.LeftOuterJoinStatement || columns.JoinDetails[i].JoinStatementType == JoinType.RightOuterJoinStatement)
                            {
                                if (columns.JoinDetails[i].CurrentJoinTableName == string.Empty)
                                    sqlQuery += " on " + string.Format("{0}.{1}", tableName, columns.JoinDetails[i].CurrentJoinColumnName);
                                else
                                    sqlQuery += " on " + string.Format("{0}.{1}", columns.JoinDetails[i].CurrentJoinTableName, columns.JoinDetails[i].CurrentJoinColumnName);

                                sqlQuery += " = " + string.Format("{0}.{1}", columns.JoinDetails[i].JoinTableName, columns.JoinDetails[i].JoinColumnName);
                            }
                        }
                    }

                    if (columns.SelectConditionString == null || columns.SelectConditionString == string.Empty)
                    {
                        foreach (ConditionColumn conditionColumns in columns.ConditionColumns)
                        {
                            if (conditionColumns.ConditionFor == StatementType.AllTypes || conditionColumns.ConditionFor == StatementType.Select)
                            {
                                sqlQuery += " where ";
                                break;
                            }
                        }

                        if (columns.ConditionColumns.Count > 1)
                        {
                            for (int i = 0; i < columns.ConditionColumns.Count; i++)
                            {
                                if (columns.ConditionColumns[i].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[i].ConditionFor == StatementType.Select)
                                {
                                    if (columns.ConditionColumns[i].ConditionColumnTable == string.Empty)
                                        sqlQuery += AddComparisonOperator(string.Format("{0}.", tableName) + columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, parameterCount, ref cmd);
                                    else
                                        sqlQuery += AddComparisonOperator(string.Format("{0}.", columns.ConditionColumns[i].ConditionColumnTable) + columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, parameterCount, ref cmd);
                                }

                                parameterCount++;

                                switch (columns.ConditionColumns[i].ConditionAndOr)
                                {
                                    case AndOrCondition.EndCondition:
                                        sqlQuery += " AND ";
                                        break;
                                    case AndOrCondition.OrCondition:
                                        sqlQuery += " OR ";
                                        break;
                                    default:
                                        sqlQuery += " AND ";
                                        defaultConditionEntered = true;
                                        break;
                                }
                            }
                            if (defaultConditionEntered == true)
                            {
                                sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf("AND"));
                            }
                        }
                        else if (columns.ConditionColumns.Count == 1)
                        {
                            if (columns.ConditionColumns[0].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[0].ConditionFor == StatementType.Select)
                            {
                                if (columns.ConditionColumns[0].ConditionColumnTable == string.Empty)
                                    sqlQuery += AddComparisonOperator(string.Format("{0}.", tableName) + columns.ConditionColumns[0].ConditionColumnName, columns.ConditionColumns[0].ConditionComparisonOperators, columns.ConditionColumns[0].ConditionColumnValue, parameterCount, ref cmd);
                                else
                                    sqlQuery += AddComparisonOperator(string.Format("{0}.", columns.ConditionColumns[0].ConditionColumnTable) + columns.ConditionColumns[0].ConditionColumnName, columns.ConditionColumns[0].ConditionComparisonOperators, columns.ConditionColumns[0].ConditionColumnValue, parameterCount, ref cmd);
                            }

                        }
                    }
                    else
                    {
                        sqlQuery += " " + columns.SelectConditionString;
                    }

                    cmd.CommandText = sqlQuery;
                    da = new MySqlDataAdapter();

                    if (isCacheEnabled.ToLower() == "true" && !(cache.Get(cacheKey) is object))
                    {
                        da.Fill(result);
                        cache.Insert(cacheKey, result);
                    }
                    else if (isCacheEnabled.ToLower() == "true" && cache.Get(cacheKey) is object)
                    {
                        result = (DataTable)cache[cacheKey];
                    }
                    else
                    {
                        da.Fill(result);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in cmd.Parameters)
                    {
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                    }
                    cmd.Dispose();
                    cmd = null;
                    da.Dispose();
                    da = null;
                }

            }
            return result;
        }

        public static bool InsertNewRecord(string sqlQuery)
        {
            bool success = false;

            try
            {
                if (UseTransaction == false)
                {
                    sqlCmd.Transaction = null;
                    sqlCmd.Connection = sqlConn;
                }
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = sqlQuery;

                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                if (sqlCmd.ExecuteNonQuery() > 0)
                    success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sqlCmd.Dispose();
                sqlCmd = null;
            }
            return success;
        }

        public static bool InsertNewRecord(string tableName, ColumnsDetail columns)
        {
            bool success = false;
            int parameterCounter = 1;
            string sqlQuery = string.Empty;
            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 0)
            {
                try
                {
                    sqlCmd.Parameters.Clear();
                    if (UseTransaction == false)
                    {
                        sqlCmd.Transaction = null;
                        sqlCmd.Connection = sqlConn;
                    }

                    sqlCmd.CommandType = CommandType.Text;

                    sqlQuery = "insert into " + tableName + " ( ";
                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        if (!columns.EliminatedColumns.Contains(columns.ColumnName[i]))
                        {
                            sqlQuery += columns.ColumnName[i] + ",";
                        }
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));
                    sqlQuery += " ) values ( ";

                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        if (!columns.EliminatedColumns.Contains(columns.ColumnName[i]))
                        {
                            sqlQuery += string.Format("@parameter{0}", parameterCounter) + " , ";
                            sqlCmd.Parameters.AddWithValue(string.Format("@parameter{0}", parameterCounter), columns.ColumnValue[i]);
                        }
                        parameterCounter++;
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));
                    sqlQuery += ")";

                    sqlCmd.CommandText = sqlQuery;

                    if (sqlConn.State != ConnectionState.Open)
                        OpenConnection();

                    if (sqlCmd.ExecuteNonQuery() > 0)
                        success = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in sqlCmd.Parameters)
                    {
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                    }
                }
            }
            return success;
        }

        public static bool InsertValuesRecord(string tableName, ColumnsDetail columns)
        {
            bool success = false;
            int parameterCounter = 1;
            string sqlQuery = string.Empty;

            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 0)
            {
                try
                {
                    sqlCmd.Parameters.Clear();
                    if (UseTransaction == false)
                    {
                        sqlCmd.Transaction = null;
                        sqlCmd.Connection = sqlConn;
                    }

                    sqlCmd.CommandType = CommandType.Text;

                    sqlQuery = "insert into " + tableName + " values(";

                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        sqlQuery += string.Format("@parameters{0}", parameterCounter) + " , ";
                        sqlCmd.Parameters.AddWithValue(string.Format("@parameters{0}", parameterCounter), columns.ColumnValue[i]);
                        parameterCounter++;
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));
                    sqlQuery += ")";

                    if (sqlConn.State != ConnectionState.Open)
                        OpenConnection();

                    sqlCmd.CommandText = sqlQuery;
                    if (sqlCmd.ExecuteNonQuery() > 0)
                    {
                        success = true;
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in sqlCmd.Parameters)
                    {
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                    }
                }
            }

            return success;
        }

        public static string GetLastInsertedID()
        {
            string result = string.Empty;
            try
            {
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = "Select LAST_INSERT_ID() as 'id'";
                sqlCmd.Parameters.Clear();
                sqlCmd.Connection = sqlConn;

                if (sqlConn.State != ConnectionState.Open)
                    OpenConnection();

                result = sqlCmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

            return result;
        }

        public static bool UpdateRecord(string tableName, ColumnsDetail columns)
        {
            bool success = false;
            string sqlQuery = string.Empty;
            int parameterCounter = 1;
            int conditionParameterCounter = 1;
            bool defaultConditionEntered = false;

            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 1)
            {
                try
                {

                    sqlCmd.Parameters.Clear();
                    if (UseTransaction == false)
                    {
                        sqlCmd.Transaction = null;
                        sqlCmd.Connection = sqlConn;
                    }

                    sqlCmd.CommandType = CommandType.Text;

                    sqlQuery = "Update " + tableName + " set ";

                    for (int i = 0; i < columns.ColumnName.Count; i++)
                    {
                        if (!columns.EliminatedColumns.Contains(columns.ColumnName[i]))
                        {
                            sqlQuery += columns.ColumnName[i] + " = " + string.Format("@parameter{0}", parameterCounter) + " , ";
                            sqlCmd.Parameters.AddWithValue(string.Format("@parameter{0}", parameterCounter), columns.ColumnValue[i]);
                        }
                        parameterCounter++;
                    }

                    sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf(","));

                    if (columns.UpdateConditionString == string.Empty || columns.UpdateConditionString == null)
                    {
                        foreach (ConditionColumn column in columns.ConditionColumns)
                        {
                            if (column.ConditionFor == StatementType.AllTypes || column.ConditionFor == StatementType.Update)
                            {
                                sqlQuery += " where ";
                                break;
                            }
                        }

                        if (columns.ConditionColumns.Count > 1)
                        {
                            for (int i = 0; i < columns.ConditionColumns.Count; i++)
                            {
                                if (columns.ConditionColumns[i].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[i].ConditionFor == StatementType.Update)
                                {
                                    sqlQuery += AddComparisonOperator(columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, conditionParameterCounter, ref sqlCmd);
                                    conditionParameterCounter++;

                                    switch (columns.ConditionColumns[i].ConditionAndOr)
                                    {
                                        case AndOrCondition.EndCondition:
                                            sqlQuery += " AND ";
                                            break;
                                        case AndOrCondition.OrCondition:
                                            sqlQuery += " OR ";
                                            break;
                                        default:
                                            sqlQuery += " AND ";
                                            defaultConditionEntered = true;
                                            break;
                                    }
                                }
                            }
                            if (defaultConditionEntered == true)
                            {
                                sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf("AND"));
                            }
                        }
                        else if (columns.ConditionColumns.Count == 1)
                        {
                            if (columns.ConditionColumns[0].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[0].ConditionFor == StatementType.Update)
                            {
                                AddComparisonOperator(columns.ConditionColumns[0].ConditionColumnName, columns.ConditionColumns[0].ConditionComparisonOperators, columns.ConditionColumns[0].ConditionColumnValue, conditionParameterCounter, ref sqlCmd);
                            }
                        }
                    }
                    else
                    {
                        sqlQuery += columns.UpdateConditionString;
                    }

                    sqlCmd.CommandText = sqlQuery;

                    if (sqlConn.State != ConnectionState.Open)
                        OpenConnection();

                    if (sqlCmd.ExecuteNonQuery() > 0)
                        success = true;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in sqlCmd.Parameters)
                    {
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                    }
                }
            }

            return success;
        }

        public static bool DeleteRecord(string tableName, ColumnsDetail columns)
        {
            bool success = false;
            string sqlQuery = string.Empty;
            int parameterCounter = 1;
            bool defaultConditionEntered = false;

            if (tableName != string.Empty && columns != null && columns.ColumnName.Count > 1 && columns.DeleteConditionString != string.Empty)
            {
                try
                {
                    if (UseTransaction == false)
                    {
                        sqlCmd.Transaction = null;
                        sqlCmd.Connection = sqlConn;
                    }

                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.Parameters.Clear();
                    sqlQuery = "Delete from " + tableName + " ";

                    if (columns.DeleteConditionString != string.Empty || columns.DeleteConditionString == null)
                    {
                        foreach (ConditionColumn column in columns.ConditionColumns)
                        {
                            if (column.ConditionFor == StatementType.AllTypes || column.ConditionFor == StatementType.Delete)
                            {
                                sqlQuery += " where ";
                                break;
                            }
                        }

                        for (int i = 0; i < columns.ConditionColumns.Count; i++)
                        {
                            if (columns.ConditionColumns[i].ConditionFor == StatementType.AllTypes || columns.ConditionColumns[i].ConditionFor == StatementType.Delete)
                            {
                                sqlQuery += AddComparisonOperator(columns.ConditionColumns[i].ConditionColumnName, columns.ConditionColumns[i].ConditionComparisonOperators, columns.ConditionColumns[i].ConditionColumnValue, parameterCounter, ref sqlCmd);
                                parameterCounter++;

                                switch (columns.ConditionColumns[i].ConditionAndOr)
                                {
                                    case AndOrCondition.EndCondition:
                                        sqlQuery += " AND ";
                                        break;
                                    case AndOrCondition.OrCondition:
                                        sqlQuery += " OR ";
                                        break;
                                    default:
                                        sqlQuery += " AND ";
                                        defaultConditionEntered = true;
                                        break;
                                }
                            }
                        }
                        if (defaultConditionEntered == true)
                        {
                            sqlQuery = sqlQuery.Substring(0, sqlQuery.LastIndexOf("AND"));
                        }
                    }
                    else
                    {
                        sqlQuery += columns.DeleteConditionString;
                    }

                    sqlCmd.CommandText = sqlQuery;

                    if (sqlConn.State != ConnectionState.Open)
                        OpenConnection();

                    if (sqlCmd.ExecuteNonQuery() > 0)
                        success = true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    CurrentSqlStatment = string.Empty;
                    CurrentSqlStatment = sqlQuery;
                    CurrentSqlParameters.Clear();
                    foreach (MySqlParameter param in sqlCmd.Parameters)
                        CurrentSqlParameters.Add(param.ParameterName, param.Value);
                }
            }

            return success;
        }

        #region Internal Helper Functions

        private static string AddComparisonOperator(string columnName, ComparisonOperator comparisonOperator, string columnValue, int parameterCount, ref MySqlCommand cmd)
        {
            string result = string.Empty;

            switch (comparisonOperator)
            {
                case ComparisonOperator.Equal:
                    result = columnName + " = " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.NotEqual:
                    result = columnName + " != " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.GreaterThan:
                    result = columnName + " > " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.GreaterThanOrEqual:
                    result = columnName + " >= " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.IsNotNull:
                    result = columnName + " is not null ";
                    break;
                case ComparisonOperator.IsNull:
                    result = columnName + " is null ";
                    break;
                case ComparisonOperator.LessThan:
                    result = columnName + " < " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.LessThanOrEqual:
                    result = columnName + " <= " + string.Format("@conditionParameter{0}", parameterCount);
                    break;
                case ComparisonOperator.Like:
                    result = columnName + " like " + string.Format("'%@conditionParameter{0}%'", parameterCount);
                    break;
                case ComparisonOperator.NotLike:
                    result = columnName + " not like " + string.Format("'%@conditionParameter{0}%'", parameterCount);
                    break;
                default:
                    result = columnName + " = " + string.Format("@conditionParameter{0}", parameterCount);
                    break;

            }
            cmd.Parameters.AddWithValue(string.Format("@conditionParameter{0}", parameterCount), columnValue);
            return result;
        }

        #endregion

    }

    #endregion


}