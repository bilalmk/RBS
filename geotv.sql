/*
SQLyog Ultimate v8.55 
MySQL - 5.7.24 : Database - geotv
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`geotv` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `geotv`;

/*Table structure for table `rb_custom_packages` */

DROP TABLE IF EXISTS `rb_custom_packages`;

CREATE TABLE `rb_custom_packages` (
  `id` bigint(20) NOT NULL,
  `packageId` bigint(20) DEFAULT NULL,
  `numberOfUsers` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_custom_packages` */

/*Table structure for table `rb_loginhistory` */

DROP TABLE IF EXISTS `rb_loginhistory`;

CREATE TABLE `rb_loginhistory` (
  `id` bigint(20) NOT NULL,
  `userid` decimal(10,0) DEFAULT NULL,
  `userIp` varchar(255) DEFAULT NULL,
  `LoginTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_loginhistory` */

/*Table structure for table `rb_managers_info` */

DROP TABLE IF EXISTS `rb_managers_info`;

CREATE TABLE `rb_managers_info` (
  `id` bigint(20) NOT NULL,
  `siteid` tinyint(4) DEFAULT NULL,
  `managerName` varchar(255) DEFAULT NULL,
  `managerEmail` varchar(255) DEFAULT NULL,
  `managerPhone` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_managers_info` */

/*Table structure for table `rb_managers_referrals` */

DROP TABLE IF EXISTS `rb_managers_referrals`;

CREATE TABLE `rb_managers_referrals` (
  `id` bigint(20) NOT NULL,
  `managerId` int(11) DEFAULT NULL,
  `refferalCode` varchar(255) DEFAULT NULL,
  `refferalCodeExpire` datetime DEFAULT NULL,
  `refferalDate` datetime DEFAULT NULL,
  `refferalStatus` tinyint(4) DEFAULT NULL,
  `clientName` varchar(255) DEFAULT NULL,
  `clientEmail` varchar(255) DEFAULT NULL,
  `clientAddress` varchar(255) DEFAULT NULL,
  `clientPhone` varchar(255) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `noOfAllowedUsers` int(11) DEFAULT NULL,
  `isSingleUser` int(11) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `managerId` (`managerId`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_managers_referrals` */

/*Table structure for table `rb_packages` */

DROP TABLE IF EXISTS `rb_packages`;

CREATE TABLE `rb_packages` (
  `id` bigint(20) NOT NULL,
  `siteid` tinyint(4) DEFAULT NULL,
  `serviceId` tinyint(4) DEFAULT NULL,
  `packageType` tinyint(4) DEFAULT NULL,
  `packageTitle` varchar(255) DEFAULT NULL,
  `packageDetails` varchar(255) DEFAULT NULL,
  `packagePrice` int(11) DEFAULT NULL,
  `packagePicture` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`),
  KEY `serviceId` (`serviceId`),
  KEY `packageType` (`packageType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_packages` */

/*Table structure for table `rb_packages_revision` */

DROP TABLE IF EXISTS `rb_packages_revision`;

CREATE TABLE `rb_packages_revision` (
  `id` bigint(20) NOT NULL,
  `packageId` tinyint(4) DEFAULT NULL,
  `packageTitle` varchar(255) DEFAULT NULL,
  `packageDetails` varchar(255) DEFAULT NULL,
  `packagePrice` int(11) DEFAULT NULL,
  `packagePicture` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_packages_revision` */

/*Table structure for table `rb_packages_type` */

DROP TABLE IF EXISTS `rb_packages_type`;

CREATE TABLE `rb_packages_type` (
  `id` bigint(20) NOT NULL,
  `packageTypeName` varchar(255) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_packages_type` */

/*Table structure for table `rb_payment_options` */

DROP TABLE IF EXISTS `rb_payment_options`;

CREATE TABLE `rb_payment_options` (
  `id` bigint(20) NOT NULL,
  `tenantId` int(11) DEFAULT NULL,
  `paymentType` int(11) DEFAULT NULL,
  `paymentTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `automatePyment` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`),
  KEY `paymentType` (`paymentType`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_payment_options` */

/*Table structure for table `rb_payment_types` */

DROP TABLE IF EXISTS `rb_payment_types`;

CREATE TABLE `rb_payment_types` (
  `id` bigint(20) NOT NULL,
  `paymentType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_payment_types` */

/*Table structure for table `rb_permissions` */

DROP TABLE IF EXISTS `rb_permissions`;

CREATE TABLE `rb_permissions` (
  `id` bigint(20) NOT NULL,
  `permissionName` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_permissions` */

/*Table structure for table `rb_promotion_package` */

DROP TABLE IF EXISTS `rb_promotion_package`;

CREATE TABLE `rb_promotion_package` (
  `packageId` bigint(20) DEFAULT NULL,
  `id` bigint(20) NOT NULL,
  `promotionPrice` int(11) DEFAULT NULL,
  `promotionCode` varchar(255) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `packageCreateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_promotion_package` */

/*Table structure for table `rb_properties` */

DROP TABLE IF EXISTS `rb_properties`;

CREATE TABLE `rb_properties` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(4) DEFAULT NULL,
  `propertyName` varchar(255) DEFAULT NULL,
  `propertyAddress` varchar(2000) DEFAULT NULL,
  `cityName` varchar(255) DEFAULT NULL,
  `stateName` varchar(255) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `propertyType` tinyint(4) DEFAULT NULL,
  `propertyDetail` text,
  `numberOfUnites` tinyint(4) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `rb_properties` */

insert  into `rb_properties`(`id`,`siteid`,`propertyName`,`propertyAddress`,`cityName`,`stateName`,`zipCode`,`propertyType`,`propertyDetail`,`numberOfUnites`,`createDate`,`updateDate`,`status`) values (5,1,'Akhbar Manzil','DMA, 3rd Floor, I.I Chundrigar road','Karachi','Sindh','75850',2,'owned by Mir Shakil Ur Rehman',35,'2019-05-16 08:45:04','2019-05-17 01:13:37',1),(4,1,'AL-Rehman Building','IMM, 3rd Floor, AL-Rehman Building, I.I Chundrigar road','Karachi','Sindh','75850',2,'This building is owned by Mir Shakil ur rehman',50,'2019-05-16 08:33:02','2019-05-16 08:33:02',1),(6,1,'Habib Bank','i.i chundrigar road','Karachi','Sindh','75950',2,'test',40,'2019-05-16 10:36:07','2019-05-17 01:21:15',1),(8,1,'1','1','1','1','1',0,'1',1,'2019-05-17 03:18:37','2019-05-17 03:18:37',1);

/*Table structure for table `rb_property_manager_relation` */

DROP TABLE IF EXISTS `rb_property_manager_relation`;

CREATE TABLE `rb_property_manager_relation` (
  `id` bigint(20) NOT NULL,
  `propertyId` int(11) DEFAULT NULL,
  `managerId` int(11) DEFAULT NULL,
  `isCam` tinyint(4) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `propertyId` (`propertyId`),
  KEY `managerId` (`managerId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_property_manager_relation` */

/*Table structure for table `rb_role_permission_relation` */

DROP TABLE IF EXISTS `rb_role_permission_relation`;

CREATE TABLE `rb_role_permission_relation` (
  `id` bigint(20) NOT NULL,
  `roleId` tinyint(4) DEFAULT NULL,
  `permissionId` tinyint(4) DEFAULT NULL,
  `grantedModule` varchar(255) DEFAULT NULL,
  `siteid` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleId` (`roleId`),
  KEY `permissionId` (`permissionId`),
  KEY `siteid` (`siteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_role_permission_relation` */

/*Table structure for table `rb_roles` */

DROP TABLE IF EXISTS `rb_roles`;

CREATE TABLE `rb_roles` (
  `id` bigint(20) NOT NULL,
  `roleName` varchar(255) DEFAULT NULL,
  `roleDescription` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_roles` */

/*Table structure for table `rb_services` */

DROP TABLE IF EXISTS `rb_services`;

CREATE TABLE `rb_services` (
  `id` bigint(20) NOT NULL,
  `typeid` tinyint(4) DEFAULT NULL,
  `serviceName` varchar(255) DEFAULT NULL,
  `serviceDescription` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `typeid` (`typeid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_services` */

/*Table structure for table `rb_services_types` */

DROP TABLE IF EXISTS `rb_services_types`;

CREATE TABLE `rb_services_types` (
  `id` bigint(20) NOT NULL,
  `siteid` tinyint(4) DEFAULT NULL,
  `Servicename` varchar(255) DEFAULT NULL,
  `serviceDescription` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_services_types` */

/*Table structure for table `rb_siteusers` */

DROP TABLE IF EXISTS `rb_siteusers`;

CREATE TABLE `rb_siteusers` (
  `id` int(11) NOT NULL,
  `companyName` varchar(255) DEFAULT NULL,
  `companyUrl` varchar(255) DEFAULT NULL,
  `companyPhone` varchar(255) DEFAULT NULL,
  `companyEmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_siteusers` */

/*Table structure for table `rb_system_user_activity` */

DROP TABLE IF EXISTS `rb_system_user_activity`;

CREATE TABLE `rb_system_user_activity` (
  `id` bigint(20) NOT NULL,
  `userIp` varchar(255) DEFAULT NULL,
  `UserActivity` varchar(255) DEFAULT NULL,
  `activityDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userIp` (`userIp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_system_user_activity` */

/*Table structure for table `rb_systemusers` */

DROP TABLE IF EXISTS `rb_systemusers`;

CREATE TABLE `rb_systemusers` (
  `id` bigint(20) NOT NULL,
  `siteid` decimal(10,0) DEFAULT NULL,
  `UserLoginId` varchar(255) DEFAULT NULL,
  `UserPassword` varchar(255) DEFAULT NULL,
  `UserName` varchar(255) DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT NULL,
  `userPhone` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `roleId` tinyint(1) DEFAULT '1',
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`),
  KEY `roleId` (`roleId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_systemusers` */

insert  into `rb_systemusers`(`id`,`siteid`,`UserLoginId`,`UserPassword`,`UserName`,`emailAddress`,`userPhone`,`createDate`,`updateDate`,`lastLogin`,`roleId`,`status`) values (1,'1','bilalmk@gmail.com','123','bilal khan','bilalmk@gmail.com','9898989',NULL,NULL,NULL,1,1);

/*Table structure for table `rb_tenant_billing` */

DROP TABLE IF EXISTS `rb_tenant_billing`;

CREATE TABLE `rb_tenant_billing` (
  `id` bigint(20) NOT NULL,
  `tenantId` int(11) DEFAULT NULL,
  `billCode` varchar(255) DEFAULT NULL,
  `billAmount` int(11) DEFAULT NULL,
  `billDate` datetime DEFAULT NULL,
  `dueDate` datetime DEFAULT NULL,
  `billingDate` datetime DEFAULT NULL,
  `adjustedAmount` int(11) DEFAULT NULL,
  `paymentStatus` tinyint(1) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `billingCycle` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_billing` */

/*Table structure for table `rb_tenant_info` */

DROP TABLE IF EXISTS `rb_tenant_info`;

CREATE TABLE `rb_tenant_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(4) DEFAULT NULL,
  `managerId` int(11) DEFAULT NULL,
  `tenantType` tinyint(4) DEFAULT NULL,
  `tenantName` varchar(255) DEFAULT NULL,
  `tenantAddress` varchar(255) DEFAULT NULL,
  `tenantPhone` varchar(255) DEFAULT NULL,
  `tenantEmail` varchar(255) DEFAULT NULL,
  `tenantStatus` tinyint(4) DEFAULT NULL,
  `tenantParentId` tinyint(4) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siteid` (`siteid`),
  KEY `managerId` (`managerId`),
  KEY `tenantType` (`tenantType`),
  KEY `tenantStatus` (`tenantStatus`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_info` */

insert  into `rb_tenant_info`(`id`,`siteid`,`managerId`,`tenantType`,`tenantName`,`tenantAddress`,`tenantPhone`,`tenantEmail`,`tenantStatus`,`tenantParentId`,`createDate`,`status`) values (1,1,1,1,'Bilal Muhammad Khan','abc','123456789','bilalmk@gmail.com',1,0,'2019-05-13 00:00:00',1);

/*Table structure for table `rb_tenant_login_creds` */

DROP TABLE IF EXISTS `rb_tenant_login_creds`;

CREATE TABLE `rb_tenant_login_creds` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tenantId` int(11) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL,
  `loginPassword` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `camLogin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_login_creds` */

insert  into `rb_tenant_login_creds`(`id`,`tenantId`,`loginName`,`loginPassword`,`status`,`lastLogin`,`camLogin`) values (1,1,'bilalmk@gmail.com','12345678',1,NULL,0);

/*Table structure for table `rb_tenant_login_history` */

DROP TABLE IF EXISTS `rb_tenant_login_history`;

CREATE TABLE `rb_tenant_login_history` (
  `id` bigint(20) NOT NULL,
  `tenantId` int(11) DEFAULT NULL,
  `loginDate` datetime DEFAULT NULL,
  `camLogin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_login_history` */

/*Table structure for table `rb_tenant_packages` */

DROP TABLE IF EXISTS `rb_tenant_packages`;

CREATE TABLE `rb_tenant_packages` (
  `id` bigint(20) NOT NULL,
  `tenantId` int(11) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  `billingCycle` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`),
  KEY `packageId` (`packageId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_packages` */

/*Table structure for table `rb_tenant_preferences` */

DROP TABLE IF EXISTS `rb_tenant_preferences`;

CREATE TABLE `rb_tenant_preferences` (
  `id` bigint(20) NOT NULL,
  `tenantId` int(11) DEFAULT NULL,
  `emailAlert` tinyint(1) DEFAULT NULL,
  `smsAlert` tinyint(1) DEFAULT NULL,
  `automatePayment` tinyint(1) DEFAULT NULL,
  `paperLessBilling` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tenantId` (`tenantId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_preferences` */

/*Table structure for table `rb_tenant_status` */

DROP TABLE IF EXISTS `rb_tenant_status`;

CREATE TABLE `rb_tenant_status` (
  `id` bigint(20) NOT NULL,
  `tenantStatusName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_status` */

/*Table structure for table `rb_tenant_type` */

DROP TABLE IF EXISTS `rb_tenant_type`;

CREATE TABLE `rb_tenant_type` (
  `id` bigint(20) NOT NULL,
  `tenantTypeName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `rb_tenant_type` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
