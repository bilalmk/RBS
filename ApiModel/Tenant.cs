﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiModel
{
    public class Tenant
    {
        public int TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Address TenantAddress { get; set; }
        public string OfficeNumber { get; set; }
        public string AccountNumber { get; set; }
        public PropertyManager TenanatManager { get; set; }
        public string Name { get { return FirstName + " " + LastName; }}
        public string Password { get; set; }
    }
}
