﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiModel
{
    public class Building
    {
        public int BuildingId { get; set; }
        public string BuildingName { get; set; }
        public Address BuildingAddress { get; set; }

    }
}
