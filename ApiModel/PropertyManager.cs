﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiModel
{
    public class PropertyManager
    {
        public int ManagerID { get; set; }
        public int SiteID { get; set; }
        public string Name { get; set; }
        public string Email{ get; set; }
        public string Phone { get; set; }      
    }
}
