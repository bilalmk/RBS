﻿using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Web.Http;
using Microsoft.Owin;
using RestApi.Token.Formats;
using RestApi.Token.Providers;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Autofac;
using Autofac.Integration.WebApi;
using ApiLibrary;
using ApiContract;

namespace RestApi
{
    public class Startup
    {

        public void Configuration(IAppBuilder app)
        {           
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            OAuthConfiguration(app);
            OAuthConfigurationValidate(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            //var container = AutofacWebapiConfig.RegisterServices(new ContainerBuilder());

            //var builder = new ContainerBuilder();
            //builder.RegisterApiControllers(System.Reflection.Assembly.GetExecutingAssembly());
            //builder.RegisterType<TenantRepository>().As<ITenantRepository>().InstancePerRequest();
            //var container = builder.Build();
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            //app.UseAutofacMiddleware(container);
            //app.UseAutofacWebApi(config);

            AutofacWebapiConfig.Initialize(config, app);

            app.UseWebApi(config);
        }

        public static void ConfigureIoc(IAppBuilder app)
        {

        }

        
        private void OAuthConfiguration(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions();
            options.TokenEndpointPath = new PathString("/oauth/token");
            options.AccessTokenFormat = new JwtTokenFormat("http://localhost/RedBison/RestApi");
            options.Provider = new OAuthProvider();
            options.AccessTokenExpireTimeSpan = TimeSpan.FromDays(10);
            options.AllowInsecureHttp = true;

            app.UseOAuthAuthorizationServer(options);
        }

        private void OAuthConfigurationValidate(IAppBuilder app)
        {
            var issure = "http://localhost/RedBison/RestApi";
            var audience = "099153c2625149bc8ecb3e85e03f0022";
            var secret = TextEncodings.Base64Url.Decode("IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw");

            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AllowedAudiences = new[] { audience },
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
                IssuerSecurityKeyProviders = new IIssuerSecurityKeyProvider[] {
                    new SymmetricKeyIssuerSecurityKeyProvider(issure,secret)
                }
            });
        }
    }
}