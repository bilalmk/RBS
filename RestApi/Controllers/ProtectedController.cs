﻿using ApiContract;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/protected")]
    public class ProtectedController : ApiController
    {
        private readonly ITenantRepository _tenantRepository;

        public ProtectedController(ITenantRepository tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }

        [HttpPost]
        [Route("")]
        public HttpResponseMessage Post()
        {
            //var tenant = _tenantRepository.FindByEmail("bilalmk@gmail.com");
            //var response = new HttpResponseMessage();
            //response.Content = new StringContent(tenant.FirstName);
            //response.StatusCode = HttpStatusCode.OK;

            var tenant = _tenantRepository.GetById(1);
            var response = new HttpResponseMessage();
            response.Content = new StringContent(tenant.FirstName);
            response.StatusCode = HttpStatusCode.OK;

            return response;
        }
    }
}
