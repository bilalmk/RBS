﻿using RestApi.Models;
using RestApi.Token.AudienceEntity;
using RestApi.Token.AudienceRepo;
using System.Web.Http;

namespace RestApi.Controllers
{
    [RoutePrefix("api/audience")]
    public class AudienceController : ApiController
    {
        [Route("")]
        public IHttpActionResult Post(AudienceModel audienceModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Audience newAudience = AudienceRepository.CreateAudience(audienceModel.Name);
            return Ok<Audience>(newAudience);
        }
    }
}
