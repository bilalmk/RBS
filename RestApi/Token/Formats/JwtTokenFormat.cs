﻿using System;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Http;
using RestApi.Token.AudienceRepo;

namespace RestApi.Token.Formats
{
    internal class JwtTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private string propertyKey = "audience";
        private string _issure = string.Empty;

        public JwtTokenFormat(string issure)
        {
            _issure = issure;
        }

        public string SignatureAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256"; }
        }

        public string DigestAlgorithm
        {
            get { return "http://www.w3.org/2001/04/xmlenc#sha256"; }
        }

        [HttpPost]
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var audienceId = data.Properties.Dictionary.ContainsKey(propertyKey) ? data.Properties.Dictionary[propertyKey] : null;

            if (string.IsNullOrWhiteSpace(audienceId))
            {
                throw new InvalidOperationException("Authentication Ticket: properties does not include audience");
            }

            var audience = AudienceRepository.FindAudience(audienceId);
            var secretKey = audience.Base64Secret;
            var bytesArray = TextEncodings.Base64Url.Decode(secretKey);

            SigningCredentials signKey = new SigningCredentials(new SymmetricSecurityKey(bytesArray), SignatureAlgorithm, DigestAlgorithm);

            var expire = data.Properties.ExpiresUtc;
            var issue = data.Properties.IssuedUtc;

            var token = new JwtSecurityToken(_issure, audienceId, data.Identity.Claims, issue.Value.UtcDateTime, expire.Value.UtcDateTime, signKey);
            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;

        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}