﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RestApi.Token.AudienceEntity
{
    public class Audience
    {
        [Key]
        [MaxLength(32)]
        public string ClientID { get; set; }

        [Required]
        [MaxLength(80)]
        public string Base64Secret { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
    }
}