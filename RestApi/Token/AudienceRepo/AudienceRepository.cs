﻿using Microsoft.Owin.Security.DataHandler.Encoder;
using RestApi.Token.AudienceEntity;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Token.AudienceRepo
{
    public static class AudienceRepository
    {
        //var issuer = ConfigurationManager.AppSettings["issuer"];


        public static ConcurrentDictionary<string, Audience> AudienceList = new ConcurrentDictionary<string, Audience>();

        static AudienceRepository()
        {
            AudienceList.TryAdd("099153c2625149bc8ecb3e85e03f0022", new Audience
            {
                ClientID = "099153c2625149bc8ecb3e85e03f0022",
                Base64Secret = "IxrAjDoa2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw",
                Name = "BSS API"
            });
        }

        public static Audience CreateAudience(string name)
        {
            var clientId = Guid.NewGuid().ToString();
            var bytes = new byte[32];
            var base64Secret = TextEncodings.Base64Url.Encode(bytes);

            var newAudience = new Audience()
            {
                ClientID = clientId,
                Base64Secret = base64Secret,
                Name = name
            };

            AudienceList.TryAdd(clientId, newAudience);
            return newAudience;
        }

        public static Audience FindAudience(string clientid)
        {
            var newAudience = new Audience();
            if (AudienceList.TryGetValue(clientid, out newAudience))
            {
                return newAudience;
            }
            else
                return null;
        }
    }
}