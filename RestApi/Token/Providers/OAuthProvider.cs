﻿using System.Threading.Tasks;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using RestApi.Token.AudienceRepo;

namespace RestApi.Token.Providers
{
    internal class OAuthProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = string.Empty;
            string clientSecretKey = string.Empty;
            string base64Key = string.Empty;

            if (!context.TryGetBasicCredentials(out clientId, out clientSecretKey))
            {
                context.TryGetFormCredentials(out clientId, out clientSecretKey);
            }

            if (context.ClientId == null)
            {
                context.SetError("invalid_clientid", "client_id is not set");
                return Task.FromResult<object>(null);
            }

            var audience = AudienceRepository.FindAudience(context.ClientId);

            if (audience == null)
            {
                context.SetError("invalid_clientid", string.Format("invalid client id '{0}'", context.ClientId));
                return Task.FromResult<object>(null);
            }

            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Controll-Allow-Origin", new[] { "*" });


            if (context.UserName != context.Password)
            {
                context.SetError("invalid_grant", "the username or password is invalid");
                return Task.FromResult<object>(null);
            }

            var identity = new ClaimsIdentity("Jwt");
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Manager"));
            identity.AddClaim(new Claim(ClaimTypes.Role, "Supervisor"));

            var prop = new AuthenticationProperties(new Dictionary<string, string>
            {
                {
                    "audience",(context.ClientId==null)?string.Empty:context.ClientId
                }
            });

            var ticket = new AuthenticationTicket(identity, prop);
            context.Validated(ticket);
            return Task.FromResult<object>(null);
        }
    }
}