﻿using ApiContract;
using ApiLibrary;
using Autofac;
using Autofac.Integration.WebApi;
using Owin;
using System.Reflection;
using System.Web.Http;

namespace RestApi
{
    public class AutofacWebapiConfig
    {

        public static IContainer Container;

        public static void Initialize(HttpConfiguration config,IAppBuilder app)
        {
            var Container = RegisterServices(new ContainerBuilder());
            Initialize(config, Container);
            app.UseAutofacMiddleware(Container);
            app.UseAutofacWebApi(config);
        }


        private static void Initialize(HttpConfiguration config, IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterType<TenantRepository>().As<ITenantRepository>().InstancePerRequest();
            //builder.RegisterType<PasswordHasher>().As<IPasswordHasher>().InstancePerRequest();
            //builder.RegisterType<TokenHandler>().As<ITokenHandler>().InstancePerRequest();
            //builder.RegisterType<AuthenticationServices>().As<IAuthenticationService>().InstancePerRequest();

            //Set the dependency resolver to be Autofac.
            Container = builder.Build();

            return Container;
        }

    }
}